
# Final Assignment
Student Name: Chun Kit Ho
Student Id: u3242002

## Installing Prerequisites

### Jupyter Notebooks with The AnacondaDistribution:
To install Jupyter, the general recommendation is to use the Anaconda distribution to install both Python and the notebook application. Please follow the instructions for the installation of Anaconda here for Mac or Windows. 

### Git Install
Please install Git

